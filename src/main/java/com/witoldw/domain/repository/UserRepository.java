package com.witoldw.domain.repository;

import java.util.List;

import com.witoldw.domain.User;

public interface UserRepository {

    User findByUserName(String userName);

    List<User> findByRole(String role);

    List<User> findAll();

    void add(User user);

    void update(User user);

    void delete(User user);
}

package com.witoldw.domain.repository.impl;

import java.util.List;

import com.witoldw.domain.User;
import com.witoldw.domain.repository.UserRepository;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDAO implements UserRepository {

    public static final String DRIVER = "org.hsqldb.jdbcDriver";
    private final String CREATE_USERS_TABLE = "CREATE TABLE IF NOT EXISTS USERS (id INTEGER AUTO_INCREMENT PRIMARY KEY, username varchar(255), pass varchar(255), permi varchar(100), email varchar(200))";
    private Connection connection = null;
    private Statement statement = null;
    private ResultSet resultSet = null;
    private PreparedStatement preparedStatement = null;

    public UserDAO() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.err.println("Brak sterownika JDBC");
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection("jdbc:mysql://robson1.me:3306/amel3", "amel", "amel123");
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            statement = connection.createStatement();
            statement.execute(CREATE_USERS_TABLE);
            statement.close();
        } catch (SQLException e) {
            System.err.println("Blad przy tworzeniu tabeli");
            e.printStackTrace();
        }
    }

    @Override
    public User findByUserName(String userName) {
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM USERS WHERE username = ?");
            preparedStatement.setString(1, userName);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.first()) {
                return new User(resultSet.getInt("id"), resultSet.getString("username"), resultSet.getString("pass"), resultSet.getString("email"), resultSet.getString("permi"));
            }
            preparedStatement.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List<User> findByRole(String role) {
        List<User> useresultSet = new ArrayList<User>();
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM USERS WHERE permi = ?");
            preparedStatement.setString(1, role);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                useresultSet.add(new User(resultSet.getInt("id"), resultSet.getString("username"), resultSet.getString("pass"), resultSet.getString("email"), resultSet.getString("permi")));
            }
            preparedStatement.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return useresultSet;
    }

    @Override
    public List<User> findAll() {
        List<User> useresultSet = new ArrayList<User>();
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM USERS");
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                useresultSet.add(new User(resultSet.getInt("id"), resultSet.getString("username"), resultSet.getString("pass"), resultSet.getString("email"), resultSet.getString("permi")));
            }
            preparedStatement.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return useresultSet;
    }

    @Override
    public void add(User user) {
        try {
            preparedStatement = connection.prepareStatement("INSERT INTO USERS(username, pass, email, permi) VALUES (? ,? ,? ,?) ");
            preparedStatement.setString(1, user.getUserName());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getRole());
            preparedStatement.execute();
            preparedStatement.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(User user) {
        try {
            preparedStatement = connection.prepareStatement("UPDATE USERS SET username = ? , pass = ? , email = ?, permi = ? WHERE id = ? ");
            preparedStatement.setString(1, user.getUserName());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getRole());
            preparedStatement.setInt(5, user.getId());
            preparedStatement.execute();
            preparedStatement.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(User user) {
        try {
            preparedStatement = connection.prepareStatement("DELETE FROM USERS WHERE id = ?");
            preparedStatement.setInt(1, user.getId());
            preparedStatement.execute();
            preparedStatement.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

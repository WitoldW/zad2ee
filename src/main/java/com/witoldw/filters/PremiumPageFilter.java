package com.witoldw.filters;

import com.witoldw.domain.User;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@WebFilter("/premium.jsp")
public class PremiumPageFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) request).getSession();
        if (session.getAttribute("userAuth") != null && (((User) session.getAttribute("userAuth")).getRole().equals("admin") || ((User) session.getAttribute("userAuth")).getRole().equals("premium"))) {
            chain.doFilter(request, response);
        } else {
            response.setContentType("text/html;  charset=UTF-8");
            response.getWriter().println("Brak uprawnień!");
        }

    }

    @Override
    public void destroy() {
        //
    }

}

package com.witoldw.services;

import com.witoldw.domain.User;
import java.util.List;

public interface UserService {
	
	User findByUserName(String userName);
        List<User> findAll();
        List<User> findByRole(String role);
	void add(User user);
	void update(User user);
	void delete(User user);
}

package com.witoldw.services.impl;

import com.witoldw.domain.User;
import com.witoldw.domain.repository.UserRepository;
import com.witoldw.domain.repository.impl.UserDAO;
import com.witoldw.services.UserService;
import java.util.List;

public class UserServiceImpl implements UserService {

    private UserRepository userRepository = null;

    public UserServiceImpl() {
        userRepository = new UserDAO();
    }

    @Override
    public User findByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }

    @Override
    public void add(User user) {
        userRepository.add(user);
    }

    @Override
    public void update(User user) {
        userRepository.update(user);

    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public List<User> findByRole(String role) {
        return userRepository.findByRole(role);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

}

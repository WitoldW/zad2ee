package com.witoldw.servlets;

import com.witoldw.domain.User;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.witoldw.services.UserService;
import com.witoldw.services.impl.UserServiceImpl;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private UserService userService;


    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        userService  = new UserServiceImpl();
    }
       

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        User user = userService.findByUserName(request.getParameter("username"));
        if (user != null && user.getPassword().equals(request.getParameter("password"))) {
            HttpSession session = request.getSession();
            session.setAttribute("userAuth", user);
            response.sendRedirect(request.getContextPath() + "/");
        } else {
            getServletContext().getRequestDispatcher("/errorLogin.jsp").forward(request, response);
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
    }

}

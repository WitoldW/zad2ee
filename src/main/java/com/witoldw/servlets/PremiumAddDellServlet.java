package com.witoldw.servlets;

import com.witoldw.domain.User;
import com.witoldw.services.UserService;
import com.witoldw.services.impl.UserServiceImpl;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "PremiumAddDellServlet", urlPatterns = {"/admin/add"})
public class PremiumAddDellServlet extends HttpServlet {

    private UserService userService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        userService = new UserServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/admin/addPremium.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User user = userService.findByUserName(request.getParameter("username"));
        if (user != null) {
            if (request.getParameter("operation").equals("add")) {
                user.setRole("premium");
            } else {
                user.setRole("user");
            }
            userService.update(user);
        }
        response.sendRedirect(request.getContextPath() + "/admin/premiumlist");
    }

}

package com.witoldw.servlets;

import com.witoldw.domain.User;
import com.witoldw.services.UserService;
import com.witoldw.services.impl.UserServiceImpl;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "PremiumListServlet", urlPatterns = {"/admin/premiumlist"})
public class PremiumListServlet extends HttpServlet {

    private UserService userService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        userService = new UserServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<User> users = userService.findAll();
        request.setAttribute("users", users);
        getServletContext().getRequestDispatcher("/admin/listOfPremium.jsp").forward(request, response);
    }

}

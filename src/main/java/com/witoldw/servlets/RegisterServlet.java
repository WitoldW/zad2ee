package com.witoldw.servlets;

import com.witoldw.domain.User;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.witoldw.services.UserService;
import com.witoldw.services.impl.UserServiceImpl;
import javax.servlet.ServletConfig;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private UserService userService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        userService = new UserServiceImpl();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (userService.findByUserName(request.getParameter("username")) == null) {
            if (request.getParameter("password").equals(request.getParameter("cpassword"))) {
                userService.add(new User(request.getParameter("username"), request.getParameter("password"), request.getParameter("email"), "user"));
                response.sendRedirect(request.getContextPath() + "/login");
            } else {
                request.setAttribute("passError", true);
                getServletContext().getRequestDispatcher("/register.jsp").forward(request, response);
            }
        } else {
            request.setAttribute("userExist", "Taki użytkownik już istnieje!");
            getServletContext().getRequestDispatcher("/register.jsp").forward(request, response);
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        getServletContext().getRequestDispatcher("/register.jsp").forward(request, response);
    }

}

<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Dodaj/usuń premium</title>
</head>
<body>
<p>Dodaj/usuń premium</p>
<form method="POST" action="add">
<table cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td align="right">Nazwa użytkownika:&nbsp;</td>
    <td>
      <input type="text" name="username">
    </td>
  </tr>
  <tr>
    <td align="right">Operacja:&nbsp;</td>
    <td>
      <input type="radio" name="operation" value="add" checked="checked" />Dodaj
      <input type="radio" name="operation" value="del"  />Usuń
    </td>
  </tr>
  <tr>
    <td></td>
    <td><input type="submit" value="Wykonaj"></td>
  </tr>
</table>
</form>
</body>
</html>
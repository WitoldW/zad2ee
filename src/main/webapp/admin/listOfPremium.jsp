<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Użytkownicy premium </title>
    </head>
    <body>
        <h1>Użytkownicy premium:</h1>
        <table cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td><b>Username</b></td>
                <td><b>Premium</b></td>
            </tr>
            <c:forEach items="${users}" var="user">
                <tr>
                    <td>${user.userName}</td>
                    <c:if test="${user.role eq 'premium'}" >
                        <td>tak</td>
                    </c:if>
                    <c:if test="${not (user.role eq 'premium')}" >
                        <td>nie</td>
                    </c:if>
                </tr>                               
            </c:forEach>
        </table>
        
        <a href="add">Dodaj/usuń premium</a>
    </body>
</html>

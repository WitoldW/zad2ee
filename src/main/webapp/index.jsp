<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Strona główna</title>
    </head>
    <body>
        <h1>Główna</h1>
        <c:if test="${not empty sessionScope.userAuth}">
            <a href="profile?user=<c:out value="${sessionScope.userAuth.userName}" />">Mój profil</a>
            <c:if test="${ (sessionScope.userAuth.role eq 'premium') or (sessionScope.userAuth.role eq 'admin') }" >
                <a href="premium.jsp">Strona premium</a>
            </c:if>
            <c:if test="${sessionScope.userAuth.role eq 'admin'}" >
                <a href="admin/premiumlist">Lista użytkowników</a>
                <a href="admin/add">Dodaj/usuń premium</a>
            </c:if>   
            <a href="logout">Logout</a>
        </c:if>
        <c:if test="${empty sessionScope.userAuth}">
            <a href="login" >Zaloguj</a>
            <a href="register">Zarejestruj</a>
        </c:if>
    </body>
</html>

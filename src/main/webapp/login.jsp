<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Logowanie</title>
</head>
<body>
<p>Wpisz login i hasło:</p>
<form method="POST" action="login">
<table cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td align="right">Nazwa użytkownika:&nbsp;</td>
    <td>
      <input type="text" name="username">
    </td>
  </tr>
  <tr>
    <td align="right">Hasło:&nbsp;</td>
    <td>
      <input type="password" name="password">
    </td>
  </tr>
  <tr>
    <td></td>
    <td><input type="submit" value="Zaloguj"></td>
  </tr>
</table>
</form>
</body>
</html>
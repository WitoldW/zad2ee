<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Profil - <c:out value="${user.userName}" /> </title>
    </head>
    <body>
        <h1>Profil - <c:out value="${user.userName}" /></h1>
        <h2>Mail: <a href="mailto:<c:out value="${user.email}" />"><c:out value="${user.email}" /></a> </h2>
    </body>
</html>

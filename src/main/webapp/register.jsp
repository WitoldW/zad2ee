<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Rejestracja</title>
    </head>
    <body>
        <p>Uzupełnij pola:</p>
        <form method="POST" action="register">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="right">Nazwa użytkownika:&nbsp;</td>
                    <td>
                        <input type="text" name="username">
                    </td>
                </tr>
                <h2><c:out value="${userExist}" /></h2>
                <tr>
                    <td align="right">Hasło:&nbsp;</td>
                    <td>
                        <input type="password" name="password">
                    </td>
                </tr>
                <tr>
                    <td align="right">Powtórz hasło:&nbsp;</td>
                    <td>
                        <input type="password" name="cpassword">
                    </td>
                </tr>
                <tr>
                    <td align="right">Email&nbsp;</td>
                    <td>
                        <input type="email" name="email">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Zarejestruj"></td>
                </tr>
            </table>
            <c:if test="${passError}"> 
                <h1>Podane hasła są różne!</h1>
            </c:if>
        </form>
    </body>
</html>